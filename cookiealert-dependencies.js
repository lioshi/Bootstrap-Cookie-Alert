$(function () {
    "use strict";

    if (!Cookies.get("yizli_accept_cookies")) {
        $(".cookiealert").addClass("show");
    }

    $(".acceptcookies").click(function () {
        Cookies.set("yizli_accept_cookies", true, { expires: cookieDurationInDays });
        $(".cookiealert").removeClass("show");
    });
});
